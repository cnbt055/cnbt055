#include <stdio.h>
int main() 
{
char ch,*p;
p=&ch;
int lowercase,uppercase;
printf("Enter an alphabet: ");
scanf("%c",p);
lowercase = (*p == 'a' || *p  == 'e' || *p  == 'i' || *p  == 'o' || *p  == 'u');
uppercase = (*p  == 'A' || *p  == 'E' || *p  == 'I' || *p  == 'O' || *p  == 'U');
if (lowercase || uppercase)
{
printf("%c is a vowel.",*p );
}
else{
printf("%c is a consonant.",*p);
}
return 0;
}

#include <stdio.h>
void swap(int *x,int *y);
int main()
{
int num1,num2;
printf("Enter value of num1: ");
scanf("%d",&num1);
printf("Enter value of num2: ");
scanf("%d",&num2);
printf("Before Swapping the two numbers are said to be: %d and %d\n",num1,num2);
swap(&num1,&num2);
printf("After  Swapping the two numbers are said to be: %d and %d\n",num1,num2);
return 0;
}
void swap(int *x,int *y)
{
int t;
t=*x;
*x=*y;
*y=t;
}