#include <stdio.h>
int main() {
    int a[20][20], transpose[20][20], r, c, i, j;
    r = 3;
    c = 3;

    
    printf("\nenter the matrix elements:\n");
    for (i = 0; i < r; ++i)
        for (j = 0; j < c; ++j) {
            printf("enter element a(%d,%d): ", i + 1, j + 1);
            scanf("%d", &a[i][j]);
        }

    
    printf("\nentered matrix: \n");
    for (i = 0; i < r; ++i)
        for (j = 0; j < c; ++j) {
            printf("%d  ", a[i][j]);
            if (j == c - 1)
                printf("\n");
        }

   
    for (i = 0; i < r; ++i)
        for (j = 0; j < c; ++j) {
            transpose[j][i] = a[i][j];
        }
    printf("\nthe transpose of the given matrix is:\n");
    for (i = 0; i < c; ++i)
        for (j = 0; j < r; ++j) {
            printf("%d  ", transpose[i][j]);
            if (j == r - 1)
                printf("\n");
        }
    return 0;
}
