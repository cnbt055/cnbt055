#include <stdio.h>
#include <stdlib.h>
#include <process.h>
int main()
{
FILE*fp;
char sentence[100];
fp=fopen("input","w");
if(fp==NULL)
{
printf("unable to open file\n");
printf("check whether the file exists\n");
exit(1);
}
printf("File opened successfully\nReading file contents:\n");
printf("enter a sentence:\n");
fgets(sentence,sizeof(sentence),stdin);
fprintf(fp,"%s",sentence);
fp=fopen("input","r");
fscanf(fp,"%[^\n]",sentence);
printf("from the file:\n%s",sentence);
fclose(fp);
return 0;
}
